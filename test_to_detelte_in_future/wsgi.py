import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'test_to_detelte_in_future.settings')

application = get_wsgi_application()
