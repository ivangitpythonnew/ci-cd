import os

from django.core.asgi import get_asgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'test_to_detelte_in_future.settings')

application = get_asgi_application()
